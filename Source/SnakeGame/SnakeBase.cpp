// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "Components/StaticMeshComponent.h"
#include "Food.h"
#include "PlayerPawnBase.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 10.f;
	LastMoveDirection = EMovementDirection::DOWN;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	AddSnakeElement(5);
	SetActorTickInterval(MovementSpeed);
	FreeDirection = true;
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void ASnakeBase::Move()
{
	FreeDirection = true;
	FVector MovementVector(ForceInitToZero);
	MovementSpeed = ElementSize;

		switch (LastMoveDirection)
		{
		case EMovementDirection::UP:
			MovementVector.X += MovementSpeed;
			break;

		case EMovementDirection::DOWN:
			MovementVector.X -= MovementSpeed;
			break;

		case EMovementDirection::LEFT:
			MovementVector.Y -= MovementSpeed;
			break;

		case EMovementDirection::RIGHT:
			MovementVector.Y += MovementSpeed;
			break;
		}

	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num()-1; i >0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);

		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
		else
		{
			this->Destroy();
		}
	}
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; i++)
	{
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, -300);
		FTransform NewTansform(NewLocation);
		ASnakeElementBase* NewSnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTansform);

		NewSnakeElement->SnakeOwner = this;

		SnakeElements.Add(NewSnakeElement);
		int32 ElemIndex = SnakeElements.Find(NewSnakeElement);
		if (ElemIndex == 0)
		{
			FVector FirstLocation(0, 0, 0);
			NewSnakeElement->SetActorLocation(FirstLocation);
			NewSnakeElement->SetFirstElementType();
		}
	}
}
