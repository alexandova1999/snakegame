// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
#include "Components/StaticMeshComponent.h"
#include <ctime>

// Sets default values
AFood::AFood()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	FoodMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	FoodMeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	FoodMeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);

}

AFood::~AFood()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}
// �������� ��� �����
void AFood::AddFood()
{
	int a = (rand() % 900 - 450);
	int b = (rand() % 900 - 450);

	FVector NewFoodLocation(a, b, 0);
	FTransform NewFoodTansform(NewFoodLocation);
	//AFood* NewFood = GetWorld()->SpawnActor<AFood>(FoodActorClass, NewFoodTansform);

	this->SetActorLocation(NewFoodLocation);
}

// �� �����
void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	//Destroy();
	AddFood();
	PlayerScore++;

	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
		}
	}
}